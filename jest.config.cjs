module.exports = {
  moduleNameMapper: {
    "^\\$lib/(.+)$": "<rootDir>/src/lib/$1",
    "^\\$app/(.+)$": "<rootDir>/mock/app/$1",
    // "^\\$service-worker$": "<rootDir>/.svelte/build/runtime/service-worker",
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ['cdk'],
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
};