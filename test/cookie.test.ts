import { ValidationResult } from '$lib/session'

test('encode and decode cookie', () => {
    const vr = new ValidationResult(
        true,
        '6fbf1729d8ed02c7d865ae137034bcc8c0df3c2d',
        'b099a24bd04678e1ea13171fb69e3470ec42cd5c',
        Math.floor(Date.now() / 1000 + 100)
    )
    const c = vr.toCookie()
    const vr2 = ValidationResult.fromCookie(c)

    expect(vr2.sessionId).toBe(vr.sessionId)
    expect(vr2.profileId).toBe(vr.profileId)
    expect(vr2.expiresAt).toBe(vr.expiresAt)
})