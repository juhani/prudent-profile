import { clearTable, } from '$lib/db'
import { checkEmailValidation, startEmailValidation, } from '$lib/session'

beforeAll(async () => {
    await clearTable()
})

test('start validation', async () => {
    const rv = await startEmailValidation('its@me.here', '10.10.10.10', 'ua')
    expect(rv.validation.email).toBe('its@me.here')
})

test('fail confirmation', async () => {
    const rv = await checkEmailValidation('lksadjflkdsajfasldfkj')
    expect(rv.ok).toBe(false)
})

test('confirm email', async () => {
    const rv = await startEmailValidation('its@me.here', '10.10.10.10', 'ua')
    const validationResult = await checkEmailValidation(rv.validation.code!)
    expect(validationResult.ok).toBe(true)
    expect(validationResult.profileId).toBeTruthy()
    expect(validationResult.sessionId).toBeTruthy()
})

test('existing is not new', async () => {
    const rv1 = await startEmailValidation('first@time.here', '10.10.10.10', 'ua')
    expect(rv1.isNew).toBeTruthy()
    const rv2 = await startEmailValidation('first@time.here', '10.10.10.11', 'ua2')
    expect(rv2.isNew).toBeFalsy()
})

