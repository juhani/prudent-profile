import type { Adapter } from '@sveltejs/kit'
import * as path from 'path'
import * as fs from 'fs'
//@ts-ignore
import ParcelBundler from "parcel-bundler";
import { spawn } from 'child_process';

const rmRecursive = (p: string) => {
    if (!fs.existsSync(p)) return
    const stats = fs.statSync(p)
    if (stats.isDirectory()) {
        fs.readdirSync(p).forEach(f => {
            rmRecursive(path.join(p, f))
        })
        fs.rmdirSync(p)
    } else {
        fs.unlinkSync(p)
    }
}

export function newAdapter(targetStack: string): Adapter {
    return {
        name: 'MAGIC',
        async adapt(utils): Promise<void> {
            const contentPath = path.join(__dirname, 'content')
            rmRecursive(contentPath)
            const serverPath = path.join(contentPath, 'server')
            const staticPath = path.join(contentPath, 'static')
            utils.copy_server_files(serverPath)
            utils.copy_client_files(staticPath)
            utils.copy_static_files(staticPath)

            const ssrBundler = new ParcelBundler(
                [path.join(__dirname, 'lambda', 'index.js')],
                {
                    outDir: path.join(contentPath, 'server-bundle'),
                    bundleNodeModules: true,
                    target: 'node',
                    sourceMaps: false,
                    minify: false,
                },
            )
            await ssrBundler.bundle()

            const emailSenderBundler = new ParcelBundler(
                [path.join(__dirname, 'lib', 'send-email-lambda', 'index.ts')],
                {
                    outDir: path.join(contentPath, 'send-email'),
                    bundleNodeModules: true,
                    target: 'node',
                    sourceMaps: false,
                    minify: false,
                },
            )
            await emailSenderBundler.bundle()

            const proc = spawn('npx', [
                'cdk',
                'deploy',
                targetStack,
                '--require-approval', 'never',
            ], {
                cwd: __dirname,
            })
            proc.stdout.pipe(process.stdout)
            proc.stderr.pipe(process.stderr)
        }
    }
}