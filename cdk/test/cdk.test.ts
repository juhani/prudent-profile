import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import { initSESClient, sendConfirmationLink } from '../lib/send-email-lambda';
import * as Cdk from '../lib/stack';

// test('Empty Stack', () => {
//     const app = new cdk.App();
//     // WHEN
//     const stack = new Cdk.PrudentProfileStack(app, 'MyTestStack', {
//       serverPath: 'serverPath',
//       staticPath: 'staticPath',
//       domain: 'domain',
//     });
//     // THEN
//     expectCDK(stack).to(matchTemplate({
//       "Resources": {}
//     }, MatchStyle.EXACT))
// });

test('send email', async () => {
  await initSESClient()
  await sendConfirmationLink({
    code: '123',
    email: process.env.TEST_EMAIL!,
  })
})