import * as cdk from '@aws-cdk/core'
import * as iam from '@aws-cdk/aws-iam'
import { PolicyStatement } from '@aws-cdk/aws-iam'

interface SendEmailRoleProps extends cdk.StackProps {
    trustedAccount: string,
    domain: string,
    rolePath: string
    roleName: string
}

export class SendEmailRoleStack extends cdk.Stack {
    public role:iam.Role

    constructor(scope: cdk.Construct, id: string, props: SendEmailRoleProps) {
        super(scope, id, props)

        this.role = new iam.Role(this, 'role', {
            assumedBy: new iam.AccountPrincipal(props.trustedAccount),
            path: props.rolePath,
            roleName: props.roleName,
            inlinePolicies: {
                'sendEmail': new iam.PolicyDocument({
                    statements: [
                        new PolicyStatement({
                            actions: ['ses:SendEmail'],
                            effect: iam.Effect.ALLOW,
                            resources: [
                                `arn:${this.partition}:ses:${this.region}:${this.account}:identity/${props.domain}`
                            ]
                        })
                    ]
                })
            }
        })
    }
}