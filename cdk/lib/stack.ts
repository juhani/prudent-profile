import * as cdk from '@aws-cdk/core'
import * as lambda from '@aws-cdk/aws-lambda'
import * as gw from '@aws-cdk/aws-apigatewayv2'
import * as s3 from '@aws-cdk/aws-s3'
import * as s3depl from '@aws-cdk/aws-s3-deployment'
import { LambdaProxyIntegration } from '@aws-cdk/aws-apigatewayv2-integrations'
import * as cdn from '@aws-cdk/aws-cloudfront'
import * as cm from '@aws-cdk/aws-certificatemanager'
import * as dns from '@aws-cdk/aws-route53'
import * as dnsTargets from '@aws-cdk/aws-route53-targets'
import * as ddb from '@aws-cdk/aws-dynamodb'
import * as secrets from '@aws-cdk/aws-secretsmanager'
import * as iam from '@aws-cdk/aws-iam'
import { DynamoEventSource } from '@aws-cdk/aws-lambda-event-sources';
import * as fs from 'fs';
import * as path from 'path'

interface PrudentProfileProps extends cdk.StackProps {
  senderAddress: string
  emailSenderPath: string
  serverPath: string
  staticPath: string
  domain: string
  zoneDomain?: string
  emailRole: string
}

export class PrudentProfileStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: PrudentProfileProps) {
    super(scope, id, props)

    const hostedZone = dns.HostedZone.fromLookup(this, 'zone', {
      domainName: props.zoneDomain || props.domain
    })

    const certificate = new cm.DnsValidatedCertificate(this, "Certificate", {
      domainName: props.domain,
      region: 'us-east-1',
      hostedZone
    });

    const table = new ddb.Table(this, 'table', {
      billingMode: ddb.BillingMode.PAY_PER_REQUEST,
      partitionKey: {
        name: 'pk',
        type: ddb.AttributeType.STRING,
      },
      sortKey: {
        name: 'sk',
        type: ddb.AttributeType.STRING,
      },
      stream: ddb.StreamViewType.NEW_AND_OLD_IMAGES,
      timeToLiveAttribute: 'expiresAt'
    })

    table.addGlobalSecondaryIndex({
      indexName: 'gsi1',
      partitionKey: {
        name: 'g1pk',
        type: ddb.AttributeType.STRING,
      },
      sortKey: {
        name: 'g1sk',
        type: ddb.AttributeType.STRING,
      },
      projectionType: ddb.ProjectionType.ALL,
    })

    const secret = new secrets.Secret(this, 'secret')

    const ssr = new lambda.Function(this, 'ssr', {
      code: new lambda.AssetCode(props?.serverPath),
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_14_X,
      memorySize: 512,
      environment: {
        TABLE: table.tableName,
        SECRET: secret.secretValue.toString(),
        HOST: props.domain,
      }
    })

    const sendEmail = new lambda.Function(this, 'sendEmail', {
      code: new lambda.AssetCode(props?.emailSenderPath),
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_14_X,
      environment: {
        LINK_DOMAIN: props.domain,
        SENDER_EMAIL: props.senderAddress,
      }
    })
    
    sendEmail.addToRolePolicy(new iam.PolicyStatement({
      actions: ['sts:AssumeRole'],
      resources: [props.emailRole],
      effect: iam.Effect.ALLOW,
    }))

    sendEmail.addEventSource(new DynamoEventSource(table, {
      startingPosition: lambda.StartingPosition.TRIM_HORIZON,
      bisectBatchOnError: true,
      retryAttempts: 2,
      batchSize: 10,
    }))

    table.grantReadWriteData(ssr)

    const api = new gw.HttpApi(this, 'api')
    api.addRoutes({
      path: '/{proxy+}',
      methods: [gw.HttpMethod.ANY],
      integration: new LambdaProxyIntegration({
        handler: ssr,
        payloadFormatVersion: gw.PayloadFormatVersion.VERSION_1_0,
      })
    })

    const staticBucket = new s3.Bucket(this, 'staticBucket')

    const staticID = new cdn.OriginAccessIdentity(this, 'staticID')
    staticBucket.grantRead(staticID)

    const distro = new cdn.CloudFrontWebDistribution(this, 'distro', {
      priceClass: cdn.PriceClass.PRICE_CLASS_100,
      defaultRootObject: '',
      viewerCertificate: {
        aliases: [props.domain],
        props: {
          acmCertificateArn: certificate.certificateArn,
          sslSupportMethod: cdn.SSLMethod.SNI,
        }
      },
      originConfigs: [
        {
          customOriginSource: {
            domainName: cdk.Fn.select(1, cdk.Fn.split('://', api.apiEndpoint)),
            originProtocolPolicy: cdn.OriginProtocolPolicy.HTTPS_ONLY,
          },
          behaviors: [
            {
              allowedMethods: cdn.CloudFrontAllowedMethods.ALL,
              forwardedValues: {
                queryString: false,
                cookies: {
                  forward: 'whitelist',
                  whitelistedNames: ['sid', 'sid.sig']
                }
              },
              isDefaultBehavior: true
            }
          ]
        },
        {
          s3OriginSource: {
            s3BucketSource: staticBucket,
            originAccessIdentity: staticID,
          },
          behaviors: mkStaticRoutes(props.staticPath)
        }
      ]
    })

    const staticDeployment = new s3depl.BucketDeployment(this, 'staticDeployment', {
      destinationBucket: staticBucket,
      sources: [s3depl.Source.asset(props.staticPath)],
      distribution: distro,
    })

    new dns.ARecord(this, 'dnsRecord', {
      recordName: props.domain,
      target: dns.RecordTarget.fromAlias(new dnsTargets.CloudFrontTarget(distro)),
      zone: hostedZone,
    })

  }
}

function mkStaticRoutes(staticPath: string): cdn.Behavior[] {
  return fs.readdirSync(staticPath).map(f => {
    const fullPath = path.join(staticPath, f)
    const stat = fs.statSync(fullPath)
    if (stat.isDirectory()) {
      return {
        pathPattern: `/${f}/*`,
      }
    }
    return { pathPattern: `/${f}` }
  })
}