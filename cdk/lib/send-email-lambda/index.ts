import { DynamoDBStreamHandler } from 'aws-lambda'
import STS from 'aws-sdk/clients/sts'
import SES from 'aws-sdk/clients/sesv2'

let ses: SES

const linkDomain = process.env.LINK_DOMAIN || 'prudent-profile.com'
const senderEmail = process.env.SENDER_EMAIL || `test <noreply-test@prudent-profile.com>`
const roleArn = process.env.ROLE_ARN || 'arn:aws:iam::385180606991:role/prudentprofile/sendemail'
const sessionName = process.env.SESSION_NAME || 'emailsender'
const identityArn = process.env.IDENTITY_ARN || 'arn:aws:ses:eu-west-1:385180606991:identity/prudent-profile.com'

export const initSESClient = async (): Promise<void> => {
    if (ses) return

    const sts = new STS({ region: 'eu-north-1' })
    const stsRes = await sts.assumeRole({
        RoleArn: roleArn,
        RoleSessionName: sessionName,
    }).promise()

    ses = new SES({
        region: 'eu-west-1',
        credentials: {
            accessKeyId: stsRes.Credentials!.AccessKeyId,
            secretAccessKey: stsRes.Credentials!.SecretAccessKey,
            sessionToken: stsRes.Credentials!.SessionToken,
            expireTime: stsRes.Credentials?.Expiration,
        }
    })
}

interface ConfirmationInfo {
    email: string
    code: string
}

export const handler: DynamoDBStreamHandler = async ({ Records }, ctx) => {
    await initSESClient()
    const confirmations = Records.filter(r =>
        (r.eventName === 'INSERT' && r.dynamodb?.NewImage?.sk.S === 'validation:')
    ).map(r => ({
        email: r.dynamodb!.NewImage!.email.S!,
        code: r.dynamodb!.NewImage!.code.S!,
    }))
    for (const c of confirmations) {
        await sendConfirmationLink(c)
    }
}

export const sendConfirmationLink = async (c: ConfirmationInfo): Promise<void> => {
    const res = await ses.sendEmail({
        Content: {
            Simple: {
                Subject: {
                    Charset: 'utf-8',
                    Data: 'prudent-profile email confirmation',
                },
                Body: {
                    Text: {
                        Charset: 'utf-8',
                        Data: `https://${linkDomain}/login/${c.code}`,
                    }
                }
            },
        },
        Destination: {
            ToAddresses: [c.email],
        },
        FromEmailAddress: senderEmail,
        FromEmailAddressIdentityArn: identityArn
    }).promise()
    console.log('sent main', JSON.stringify(res))
}