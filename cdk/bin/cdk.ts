#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { PrudentProfileStack } from '../lib/stack';
import { SendEmailRoleStack } from '../lib/send-email-role';
import { join } from 'path'

const app = new cdk.App();

/**
 * path to bundled SvelteKit SSR code
 */
const serverPath = join(__dirname, '..', 'content', 'server-bundle')
/**
 * path to static content from SvelteKit
 */
const staticPath = join(__dirname, '..', 'content', 'static')


const emailSenderPath = join(__dirname, '..', 'content', 'send-email')

const emailRolePath = '/prudentprofile/'
const emailRoleName = 'sendemail'
const emailRole = `arn:aws:iam::${process.env.EMAIL_ACCOUNT}:role${emailRolePath}${emailRoleName}`

const sendEmailRoleStack = new SendEmailRoleStack(app, 'SendPrudenProfileEmail', {
  env: {
    account: process.env.EMAIL_ACCOUNT,
    region: process.env.EMAIL_REGION,
  },
  domain: 'prudent-profile.com',
  trustedAccount: process.env.ACCOUNT!,
  roleName: emailRoleName,
  rolePath: emailRolePath,
})

new PrudentProfileStack(app, 'PrudentProfileStack', {
  env: {
    account: process.env.ACCOUNT,
    region: process.env.REGION,
  },
  serverPath,
  staticPath,
  domain: 'prudent-profile.com',
  emailSenderPath,
  emailRole,
  senderAddress: 'prudent-profile <noreply@prudent-profile.com>',
});


new PrudentProfileStack(app, 'TestPrudentProfileStack', {
  env: {
    account: process.env.ACCOUNT,
    region: process.env.REGION,
  },
  serverPath,
  staticPath,
  domain: 'test.prudent-profile.com',
  zoneDomain: 'prudent-profile.com',
  emailSenderPath,
  emailRole,
  senderAddress: 'prudent-profile (TEST) <noreply@prudent-profile.com>',
});
