import type { GetContext, GetSession } from '@sveltejs/kit'
import { SessionInfo, ValidationResult } from '$lib/session'

export const getContext: GetContext = ({ headers }) => {
    const cookieHeader = headers.Cookie || headers.cookie
    if (!cookieHeader) return new ValidationResult(false)
    return ValidationResult.fromCookie(cookieHeader)
}

export const getSession: GetSession<ValidationResult, SessionInfo> = ({ context }) => {
    return {
        ok: context.ok,
        expiresAt: context.ok ? new Date(context.expiresAt * 1000) : undefined
    }
}
