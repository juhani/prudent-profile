
import type { RequestHandler } from '@sveltejs/kit'
import type { ValidationResult } from '$lib/session'
import { table } from '$lib/db'

export const get: RequestHandler<ValidationResult, any> = async ({ context }) => {
    if (!context || !context.ok) {
        return {
            status: 401
        }
    }
    const profileP = table.get('Profile', {
        id: context.profileId
    })
    
    const sessionsP = table.queryItems({
        pk: `profile:${context.profileId}`,
        sk: { begins: 'session:' }
    })

    const [profile, sessions] = await Promise.all([profileP, sessionsP])
    
    sessions.forEach(s => {
        delete s.pk
        delete s.sk
        s.updated = new Date(s.updated)
    })
    
    return {
        body: {
            profile,
            sessions,
        }
    }
}
