import { serialize } from 'cookie'
import type { RequestHandler } from '@sveltejs/kit'
import type { ValidationResult } from '$lib/session'

export const get: RequestHandler<ValidationResult, any> = async ({context}) => {
    if(context && context.ok) {
        await context.invalidate()
    }
    return {
        status: 307,
        headers: {
            location: '/',
            'cache-control': 'no-store, max-age=0',
            'set-cookie': serialize('sid', 'logout', {
                expires: new Date(Date.now() - 10000),
                maxAge: -1000,
                httpOnly: true,
                path: '/',
                sameSite: 'strict',
            })
        },
        body: ''
    }
}
