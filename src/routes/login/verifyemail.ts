import { startEmailValidation } from '$lib/session'
import type { RequestHandler } from '@sveltejs/kit'

interface ValidationRequest {
    email: string
}

const parseIP = (s: string): string => {
    return s ? s.split(',')[0] : 'unknown'
}

export const post: RequestHandler<any, ValidationRequest> = async ({ body, headers }) => {
    console.info(JSON.stringify(headers))
    const ua = headers['user-agent'] || 'unknown'
    const ip = parseIP(headers['X-Forwarded-For'])
    const res = await startEmailValidation(body.email, ip, ua)
    if (res.isNew) {
        return {
            status: 201,
            body: { ok: true },
            headers: {
                'cache-control': 'no-store, max-age=0'
            }
        }    
    }
    return {
        body: {
            ok: false,
            reason: 'already_exists',
            expiresAt: res.validation.expiresAt
        }
    }
}