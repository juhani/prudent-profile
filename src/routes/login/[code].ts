import { checkEmailValidation } from '$lib/session'
import type { RequestHandler } from '@sveltejs/kit'

export const get: RequestHandler = async ({ params }) => {
    const { code } = params
    const res = await checkEmailValidation(code)
    if (res.ok) {
        return {
            status: 307,
            headers: {
                location: '/',
                'cache-control': 'no-store, max-age=0',
                'set-cookie': res.toCookie()
            },
            body: ''
        }
    }

    return {
        status: 307,
        headers: {
            location: '/login/invalidcode',
            'cache-control': 'no-store, max-age=0',
        },
        body: ''
    }
}