import { Entity, Model, Table } from 'dynamodb-onetable'
import * as DDB from 'aws-sdk/clients/dynamodb'
import { randomBytes } from 'crypto';
import { emailRE } from '$lib/common/email'
import { dev } from '$app/env'

const tableName = dev ? 'prudent-profile-dev' : process.env['TABLE']

const client = new DDB.DocumentClient({
    region: 'eu-north-1',
})

/**
 * Generate a cryptographically random id 
 * 
 * @param n number of chacarters in the id
 * @returns random string with n characters
 */
export function mkID(n: number): string {
    return randomBytes(Math.ceil(n / 2)).toString('hex').substr(0, n)
}

/**
 * 
 * @returns current epoch in seconds (suitable for dynamodb TTL)
 */
export function epochSecondsNow(): number { return Math.floor(Date.now() / 1000) }

const schema = {
    indexes: {
        primary: { hash: 'pk', sort: 'sk' },
        gsi1: { hash: 'g1pk', sort: 'g1sk' }
    },
    models: {
        Profile: {
            pk: { type: String, value: 'profile:${id}' },
            sk: { type: String, value: 'profile:' },
            g1pk: { type: String, value: 'email:${email}' },
            g1sk: { type: String, value: 'email:${email}' },
            id: { type: String, default: () => mkID(40) },
            name: { type: String, required: true },
            email: { type: String, required: true, validate: emailRE },
        },
        EmailValidation: {
            pk: { type: String, value: 'validation:${code}' },
            sk: { type: String, value: 'validation:' },
            g1pk: { type: String, value: 'validation:${email}' },
            g1sk: { type: String, value: 'validation:${email}' },
            code: { type: String, default: () => mkID(40) },
            initiatingIP: { type: String, required: true },
            initiatingUA: { type: String, required: true },
            email: { type: String, required: true, validate: emailRE },
            expiresAt: { type: Number, default: () => (epochSecondsNow() + 24 * 60 * 60) }
        },
        Session: {
            pk: { type: String, value: 'profile:${profileId}' },
            sk: { type: String, value: 'session:${id}' },
            id: { type: String, default: () => mkID(40) },
            profileId: { type: String, required: true },
            initiatingIP: { type: String, required: true },
            initiatingUA: { type: String, required: true },
            expiresAt: { type: Number, default: () => (epochSecondsNow() + 30 * 24 * 60 * 60) }
        },
    }
}

export type Profile = Entity<typeof schema.models.Profile>
export type EmailValidation = Entity<typeof schema.models.EmailValidation>
export type Session = Entity<typeof schema.models.Session>

export const table = new Table({
    client,
    name: tableName,
    schema,
    timestamps: true,
})

export const clearTable = async () => {
    if (!dev) return // clear only dev table
    const records = await client.scan({
        TableName: 'prudent-profile-dev',
        AttributesToGet: ['pk', 'sk']
    }).promise()
    for (const item of records.Items) {
        await client.delete({
            TableName: 'prudent-profile-dev',
            Key: item,
        }).promise()
    }
}

export const ProfileModel:Model<Profile> = table.getModel('Profile')
export const EmailValidationModel: Model< EmailValidation > = table.getModel('EmailValidation')
export const SessionModel:Model<Session> = table.getModel('Session')
