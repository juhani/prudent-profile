import {
    EmailValidation,
    EmailValidationModel,
    epochSecondsNow,
    mkID,
    Profile,
    ProfileModel,
    SessionModel,
    table,
} from "$lib/db";
import { serialize, parse } from 'cookie'
import { createHmac } from 'crypto'
import { dev } from '$app/env'

const secret = dev ? 'verysecret' : process.env['SECRET']

/**
 * Info about session status and expiration
 */
export interface SessionInfo {
    ok: boolean
    expiresAt?: Date
}

/**
 * holds result of email confirmation and (de)serializes it to/from cookie
 */
export class ValidationResult {
    ok: boolean
    profileId?: string
    sessionId?: string
    expiresAt?: number

    constructor(ok: boolean, profileId?: string, sessionId?: string, expiresAt?: number) {
        this.ok = ok
        this.expiresAt = expiresAt
        this.profileId = profileId
        this.sessionId = sessionId
    }

    static fromCookie(c: string): ValidationResult {

        const cookies = parse(c)

        if (!cookies || !cookies.sid) {
            console.log('no session cookie')
            return new ValidationResult(false)
        }

        const [profileId, sessionId, strExpiresAt, sig1] = cookies['sid'].split('.')

        const expiresAt = parseInt(strExpiresAt, 10)

        if (expiresAt < Date.now() / 1000) {
            console.log('expired session', sessionId)
            return new ValidationResult(false)
        }

        const payload = [
            profileId,
            sessionId,
            strExpiresAt
        ].join('.')

        const sig2 = createHmac('sha256', secret).update(payload).digest('hex')

        if (sig1 !== sig2) {
            console.log('invalid signature')
            return new ValidationResult(false)
        }

        return new ValidationResult(true, profileId, sessionId, expiresAt)
    }

    toCookie(): string {
        if (!this.ok) { return '' }

        const payload = [
            this.profileId,
            this.sessionId,
            this.expiresAt.toString()
        ].join('.')

        const signature = createHmac('sha256', secret).update(payload).digest('hex')

        return serialize('sid', [payload, signature].join('.'), {
            expires: new Date(this.expiresAt * 1000),
            maxAge: this.expiresAt - Date.now() / 1000,
            path: '/',
            sameSite: 'strict',
            httpOnly: true,
        })
    }

    async invalidate(): Promise<void> {
        if (!this.ok) return

        await SessionModel.deleteItem({
            profileId: this.profileId,
            id: this.sessionId,
        })
    }
}

interface StartValidationResult {
    isNew: boolean
    validation: EmailValidation
}

export async function startEmailValidation(email: string, initiatingIP: string, initiatingUA: string): Promise<StartValidationResult> {

    const emailValidation: EmailValidation = {
        email, initiatingUA, initiatingIP,
    }

    const pending = await EmailValidationModel.get({ email }, { index: 'gsi1' })
    const validation = !pending ? await EmailValidationModel.create(emailValidation) : pending
    return {
        isNew: !pending,
        validation
    }

}

export async function checkEmailValidation(code: string): Promise<ValidationResult> {
    const res = new ValidationResult(false)
    let batch: object = {}

    // find email validation by code
    const validationKey: EmailValidation = { code }
    const emailValidation = await EmailValidationModel.getItem(validationKey)
    if (!emailValidation) {
        return res
    }
    res.ok = true

    // find profile with confirmed email
    const profileKey: Profile = { email: emailValidation.email }
    let profile = await ProfileModel.get(profileKey, {
        index: 'gsi1'
    })

    // create profile if it doesn't exist
    res.profileId = profile ? profile.id : mkID(40)
    if (!profile) {
        profile = await ProfileModel.create({
            id: res.profileId,
            email: emailValidation.email,
            name: emailValidation.email!.split('@')[0],
        }, { batch })
    }

    // create session
    res.sessionId = mkID(40)
    res.expiresAt = epochSecondsNow() + 30 * 24 * 60 * 60
    const session = await SessionModel.create({
        initiatingIP: emailValidation.initiatingIP,
        initiatingUA: emailValidation.initiatingUA,
        profileId: res.profileId,
        expiresAt: res.expiresAt,
        id: res.sessionId,
    }, { batch })

    // remove email validation record
    await EmailValidationModel.deleteItem(emailValidation, { batch })

    // run the above modification in a batch
    await table.batchWrite(batch)

    return res
}